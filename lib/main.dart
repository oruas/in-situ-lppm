import 'dart:convert';
import 'dart:io';
import 'package:flutter/material.dart';
import 'package:temporaldb/platform.dart';
import 'package:tuple/tuple.dart';

import 'package:wakelock/wakelock.dart';

import 'package:temporaldb/gps_db.dart';
import 'package:temporaldb/geo_example/utils.dart';
import 'package:temporaldb/geo_example/poi.dart';
import 'package:temporaldb/geo_example/promesse.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'In-situ LPPMs',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: const MyHomePage(title: 'In-situ LPPMs'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key, required this.title}) : super(key: key);
  final String title;
  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  /// The variable operation is used to prevent to launch two experiments
  /// at the same time. For simplicity we do not use token and assume sequential
  /// accesses to the variable. Values:
  /// -1 : interrupting the current experiment
  /// 0 : default value, no operations are currently done
  /// 1 : running the cabspotting experiment
  /// 2 : running the cabspotting experiment and changing the error
  /// 3 : computing POI on privamov, user 1
  /// 4 : loading the full privamov dataset in a FLI database
  int _operation = 0;
  String _getNameXP(int nbOperation) {
    switch (nbOperation) {
      case -1:
        return 'Interrupting experiment...';
      case 0:
        return 'No experiment running';
      case 1:
        return 'Cabspotting: storage + POI + Promesse';
      case 2:
        return 'Cabspotting: changing the error of the model';
      case 3:
        return 'Computing POI on Privamov user 1';
      case 4:
        return 'Loading privamov full dataset';
      case 5:
        return 'Cabspotting: testing the new POI attack';
      default:
        return 'Error on operation number';
    }
  }

  /// Directory where the results are stored
  static const _addResults = 'results/';

  /// Parameters for geolocation algorithms (cf POI algo)
  /// - _minTime: a time threshold which represents the minimum time that has
  ///  to be spent in every stay; expressed in seconds (s)
  /// - _maxDistance: a distance threshold representing the maximal diameter of
  /// the stay area. The distance should be expressed in meters (m).
  /// - _minPts: a minimum number of points necessary to form a cluster. This is
  /// the number of different point we have inside a cluster to maintain it.
  final double _minTime = 60 * 5;

  /// 5 minutes
  final double _maxDistance = 500;
  final int _minPts = 2;

  /// Epsilon is the distance, in meters, between two GPS point in the
  /// anonymized trace produced by Promesse
  final double _epsilon = 500;

  /// Parameters for datasets
  final String _datasetsAdd = 'datasets/';
  final String _cabspottingFileName = 'cabspotting';
  final String _privamovFileNameUser1 = 'privamov-gps-user1';
  final String _privamovFileName = 'privamov-gps';

  /// List of the errors we test with cabspotting
  List<double> errors = [0.0001, 0.0005, 0.001, 0.002, 0.005, 0.01];

  @override
  void initState() {
    super.initState();

    /// The following line will enable the Android and iOS wakelock.
    Wakelock.enable();
  }

  void _interruptXP() {
    _operation = -1;
    setState(() {});
  }

  /// ********** COMPUTING CABSPOTTING **************
  void _doCabspottingXP() async {
    if (_operation != 0) {
      return;
    }
    _operation = 1;
    setState(() {});
    debugPrint('Starting experiment on cabspotting');

    final path = await localPath;
    Map<int, List<Tuple3<double, double, double>>> gpsDataset = {};
    try {
      gpsDataset = readCSV('$path/$_datasetsAdd$_cabspottingFileName');
    } catch (e) {
      _operation = 0;
      setState(() {});
      debugPrint('Error while loading the dataset: $e');
      debugPrint('Experiment on cabspotting: aborted');
      return;
    }
    debugPrint('Dataset loaded: ${gpsDataset.length} users');

    /// List of timestamps to regenerate the trace
    List<double> timestamps = [];

    /// Variables to compute the total gain in terms of space
    double totalRaw = 0;
    double totalModeled = 0;
    double rawSize;
    double modelSize;
    List<Tuple3<double, double, double>> gpsTraceUser = [];
    int nbPromesseNonNull = 0;

    /// We keep the gain and distances from each user to compute the distribution
    List<double> gains = [];
    List<double> distances = [];

    /// We iterate each user
    for (var entry in gpsDataset.entries) {
      gpsTraceUser.clear();
      timestamps.clear();
      GPSDB gpsModel = GPSDB();

      /// We model the real trace by adding each point sequentially in the model
      /// ******************** warning *********************
      /// We reverse the trace as cabspotting is store in reverse order
      /// **************************************************
      for (var point in entry.value.reversed) {
        gpsTraceUser.add(point);
        gpsModel.add(point.item1, point.item2, point.item3);
        timestamps.add(point.item1);
      }
      var modeledTrace = gpsModel.getTrace(timestamps);

      var poiRaw = POI.getPOI(gpsTraceUser, _minTime, _maxDistance, _minPts);
      List<Tuple3<double, double, double>> gpsTraceUserPromesse =
          Promesse.smoothSpeed(gpsTraceUser, _epsilon);
      var poiPromesseRaw =
          POI.getPOI(gpsTraceUserPromesse, _minTime, _maxDistance, _minPts);

      var poiModeled =
          POI.getPOI(modeledTrace, _minTime, _maxDistance, _minPts);
      List<Tuple3<double, double, double>> modeledTracePromesse =
          Promesse.smoothSpeed(modeledTrace, _epsilon);
      var poiPromesseModeled =
          POI.getPOI(modeledTracePromesse, _minTime, _maxDistance, _minPts);

      rawSize = gpsTraceUser.length * 3;
      modelSize = gpsModel.getSize().toDouble();
      totalRaw += rawSize;
      totalModeled += modelSize;

      if (!(poiPromesseRaw.length < poiPromesseModeled.length)) {
        nbPromesseNonNull++;
      }

      double gain = (100 * (rawSize - modelSize)) / rawSize;
      gains.add(gain);

      distances += distributionDistancePOI(poiModeled, poiRaw);

      debugPrint('User ${entry.key} done');
      setState(() {});
      if (_operation == -1) {
        debugPrint('Interrupting experiment on cabspotting...');
        break;
      }
    }

    /// End parsing dataset

    /// Computing the distribution of the gains and distances of POI
    List<Tuple2<double, double>> cumulativeDistributionDistances =
        cumulativeDistribution(distances);
    List<Tuple2<double, double>> cumulativeDistributionGains =
        cumulativeDistribution(gains);

    /// Printing the results
    String add = '${_addResults}Cabspotting/xp--${getTimestamp()}/';
    File distancesFile = await localDirFile(add, 'distances.txt');
    distancesFile.writeAsStringSync(cumulativeDistributionDistances.toString());
    File gainsFile = await localDirFile(add, 'gains.txt');
    gainsFile.writeAsStringSync(cumulativeDistributionGains.toString());
    File resultsFile = await localDirFile(add, 'results.txt');
    resultsFile.writeAsStringSync(
        '{"total gain":${(100 * (totalRaw - totalModeled)) / totalRaw}, "nbPromesseFail": $nbPromesseNonNull}');

    _operation = 0;
    setState(() {});
    debugPrint('Experiment on cabspotting: done');

    return;
  }

  /// ********** COMPUTING CABSPOTTING WITH PARAMETER **************
  Future<void> _doCabspottingXPErrorAux(
      Map<int, List<Tuple3<double, double, double>>> gpsDataset,
      double error) async {
    debugPrint('Starting experiment on cabspotting with parameters $error');

    /// List of timestamps to regenerate the trace
    List<double> timestamps = [];

    /// Variables to compute the total gain in terms of space
    double totalRaw = 0;
    double totalModeled = 0;
    double rawSize;
    double modelSize;
    List<Tuple3<double, double, double>> gpsTraceUser = [];
    int nbPromesseNonNull = 0;

    /// We keep the gain and distances from each user to compute the distribution
    List<double> gains = [];
    List<double> distances = [];

    /// We iterate each user
    for (var entry in gpsDataset.entries) {
      gpsTraceUser.clear();
      timestamps.clear();
      GPSDB gpsModel = GPSDB(0, {'error': '$error'});

      /// We model the real trace by adding each point sequentially in the model
      /// ******************** warning *********************
      /// We reverse the trace as cabspotting is store in reverse order
      /// **************************************************
      for (var point in entry.value.reversed) {
        gpsTraceUser.add(point);
        gpsModel.add(point.item1, point.item2, point.item3);
        timestamps.add(point.item1);
      }
      var modeledTrace = gpsModel.getTrace(timestamps);

      var poiRaw = POI.getPOI(gpsTraceUser, _minTime, _maxDistance, _minPts);
      List<Tuple3<double, double, double>> gpsTraceUserPromesse =
          Promesse.smoothSpeed(gpsTraceUser, _epsilon);
      var poiPromesseRaw =
          POI.getPOI(gpsTraceUserPromesse, _minTime, _maxDistance, _minPts);

      var poiModeled =
          POI.getPOI(modeledTrace, _minTime, _maxDistance, _minPts);
      List<Tuple3<double, double, double>> modeledTracePromesse =
          Promesse.smoothSpeed(modeledTrace, _epsilon);
      var poiPromesseModeled =
          POI.getPOI(modeledTracePromesse, _minTime, _maxDistance, _minPts);

      rawSize = gpsTraceUser.length * 3;
      modelSize = gpsModel.getSize().toDouble();
      totalRaw += rawSize;
      totalModeled += modelSize;

      if (!(poiPromesseRaw.length < poiPromesseModeled.length)) {
        nbPromesseNonNull++;
      }

      double gain = (100 * (rawSize - modelSize)) / rawSize;
      gains.add(gain);

      distances += distributionDistancePOI(poiModeled, poiRaw);

      debugPrint('User ${entry.key} done');
      setState(() {});
      if (_operation == -1) {
        debugPrint(
            'Interrupting experiment on cabspotting with parameters $error...');
        return;
      }
      // break; /// TODO: remove this when tests are over
    }

    /// End parsing dataset

    /// Computing the distribution of the gains and distances of POI
    List<Tuple2<double, double>> cumulativeDistributionDistances =
        cumulativeDistribution(distances);
    List<Tuple2<double, double>> cumulativeDistributionGains =
        cumulativeDistribution(gains);

    /// Printing the results
    String add =
        '${_addResults}Cabspotting/$error/xp--$error--${getTimestamp()}/';
    File distancesFile = await localDirFile(add, 'distances.txt');
    distancesFile.writeAsStringSync(cumulativeDistributionDistances.toString());
    File gainsFile = await localDirFile(add, 'gains.txt');
    gainsFile.writeAsStringSync(cumulativeDistributionGains.toString());
    File resultsFile = await localDirFile(add, 'results.txt');
    resultsFile.writeAsStringSync(
        '{"total gain":${(100 * (totalRaw - totalModeled)) / totalRaw}, "nbPromesseFail": $nbPromesseNonNull}');

    debugPrint('Experiment on cabspotting with parameters $error: done');
    return;
  }

  /// ********** COMPUTING CABSPOTTING WITH DIFFERENT ERRORS **************
  void _doCabspottingXPError() async {
    if (_operation != 0) {
      return;
    }
    _operation = 2;
    setState(() {});
    debugPrint('Starting experiment on cabspotting');

    final path = await localPath;
    Map<int, List<Tuple3<double, double, double>>> gpsDataset = {};
    try {
      gpsDataset = readCSV('$path/$_datasetsAdd$_cabspottingFileName');
    } catch (e) {
      _operation = 0;
      setState(() {});
      debugPrint('Error while loading the dataset: $e');
      debugPrint('Experiment on cabspotting: aborted');
      return;
    }
    debugPrint('Dataset loaded: ${gpsDataset.length} users');

    for (var error in errors) {
      await _doCabspottingXPErrorAux(gpsDataset, error);
      if (_operation == -1) {
        break;
      }
    }

    _operation = 0;
    setState(() {});
    debugPrint('Experiment on cabspotting with errors: done');
    return;
  }

  /// **************** Cabspotting: computing POI ********************
  /// With and without our new POI attack.
  /// FLI is not used.
  void _doCabspottingXPPOIAttack() async {
    if (_operation != 0) {
      return;
    }
    _operation = 5;
    setState(() {});
    debugPrint('Starting experiment on our new POI attack on cabspotting');

    final path = await localPath;
    Map<int, List<Tuple3<double, double, double>>> gpsDataset = {};
    try {
      gpsDataset = readCSV('$path/$_datasetsAdd$_cabspottingFileName');
    } catch (e) {
      _operation = 0;
      setState(() {});
      debugPrint('Error while loading the dataset: $e');
      debugPrint('Experiment on cabspotting: aborted');
      return;
    }
    debugPrint('Dataset loaded: ${gpsDataset.length} users');

    /// List of timestamps to regenerate the trace
    List<double> timestamps = [];

    /// Variables to compute the total gain in terms of space
    List<Tuple3<double, double, double>> gpsTraceUser = [];
    // int nbPromesseNonNull=0;
    /// We keep the gain and distances from each user to compute the distribution
    List<double> distances = [];

    /// We iterate each user
    for (var entry in gpsDataset.entries) {
      gpsTraceUser.clear();
      timestamps.clear();

      /// We model the real trace by adding each point sequentially in the model
      /// ******************** warning *********************
      /// We reverse the trace as cabspotting is store in reverse order
      /// **************************************************
      for (var point in entry.value.reversed) {
        gpsTraceUser.add(point);
        timestamps.add(point.item1);
      }

      var poiRaw = POI.getPOI(gpsTraceUser, _minTime, _maxDistance, _minPts);
      List<Tuple3<double, double, double>> gpsTraceUserPromesse =
          Promesse.smoothSpeed(gpsTraceUser, _epsilon);
      var poiPromesseRaw =
          POI.getPOI(gpsTraceUserPromesse, _minTime, _maxDistance, _minPts);

      var poiNewAttack =
          POI.getTopDownPOI(gpsTraceUser, _minTime, _maxDistance, _minPts);
      var poiPromesseModeled =
          POI.getTopDownPOI(gpsTraceUser, _minTime, _maxDistance, _minPts);

      /// TODO: update to include a new metric
      // if(!(poiPromesseRaw.length < poiPromesseModeled.length)) {
      //   nbPromesseNonNull++;
      // }

      distances += distributionDistancePOI(poiNewAttack, poiRaw);

      debugPrint('User ${entry.key} done');
      setState(() {});
      if (_operation == -1) {
        debugPrint('Interrupting experiment on cabspotting...');
        break;
      }
    }

    /// End parsing dataset

    /// Computing the distribution of the gains and distances of POI
    List<Tuple2<double, double>> cumulativeDistributionDistances =
        cumulativeDistribution(distances);

    /// Printing the results
    String add = '${_addResults}Cabspotting/POIAttack/xp--${getTimestamp()}/';
    File distancesFile = await localDirFile(add, 'distances.txt');
    distancesFile.writeAsStringSync(cumulativeDistributionDistances.toString());
    // File numberPOIFile = await _localDirFile(add, 'numbersPOI.txt');
    // distancesFile.writeAsStringSync();

    _operation = 0;
    setState(() {});
    debugPrint(
        'Starting experiment on our new POI attack on cabspotting: done');
    return;
  }

  /// ********** COMPUTING POI ON PRIVAMOV USER 1 **************
  void _doPrivamovUser1() async {
    if (_operation != 0) {
      return;
    }
    _operation = 3;
    setState(() {});
    debugPrint('Starting experiment on privamov user 1');

    /// We wait one second to be sure the UI has been updated
    await Future.delayed(const Duration(seconds: 1));

    final path = await localPath;
    List<Tuple3<double, double, double>> gpsDatasetUser1 = [];
    try {
      debugPrint('Starting reading dataset...');
      gpsDatasetUser1 =
          readCSVUser('$path/$_datasetsAdd$_privamovFileNameUser1', 1);
      debugPrint('Reading dataset done!');
    } catch (e) {
      _operation = 0;
      setState(() {});
      debugPrint('Error while loading the dataset: $e');
      debugPrint('Experiment on privamov user 1: aborted');
      return;
    }
    debugPrint('Trace of user 1 loaded: ${gpsDatasetUser1.length} points');

    /// Computation time for normal and optimized POI algorithms, in milliseconds
    int normalPOITime;
    int optiPOITime;
    int initialSize = gpsDatasetUser1.length * 3;
    int modeledSize;

    /// We wait one second to be sure everything is ready
    await Future.delayed(const Duration(seconds: 1));

    debugPrint('Starting experiment!');

    /// Starting the experiment
    Stopwatch stopwatch = Stopwatch()..start();
    var poi =
        POI.getTopDownPOI(gpsDatasetUser1, _minTime, _maxDistance, _minPts);
    optiPOITime = stopwatch.elapsedMilliseconds;
    debugPrint('Computing advanced POIs done: ${optiPOITime / 1000.0} seconds');

    /// We wait one second to be sure everything is ready
    await Future.delayed(const Duration(seconds: 1));
    stopwatch.reset();
    poi = POI.getPOI(gpsDatasetUser1, _minTime, _maxDistance, _minPts);
    normalPOITime = stopwatch.elapsedMilliseconds;
    debugPrint('Computing raw POIs done: ${normalPOITime / 1000.0} seconds');

    /// We wait one second to be sure everything is ready
    await Future.delayed(const Duration(seconds: 1));
    GPSDB gpsModel = GPSDB();

    /// List of timestamps to regenerate the trace
    List<double> timestamps = [];

    /// No reverse as privamov is stored in chronological order
    for (var point in gpsDatasetUser1) {
      gpsModel.add(point.item1, point.item2, point.item3);
      timestamps.add(point.item1);
    }
    gpsDatasetUser1.clear();
    debugPrint('Modeling dataset done');

    /// We wait one second to be sure everything is ready
    await Future.delayed(const Duration(seconds: 1));

    /// In order to save space we reuse the same list
    modeledSize = gpsModel.getSize();
    var modeledTrace = gpsModel.getTrace(timestamps);
    var modeledPOI =
        POI.getTopDownPOI(modeledTrace, _minTime, _maxDistance, _minPts);
    debugPrint('Computing modeled POIs done');

    /// Printing the results
    String add = '${_addResults}Privamov/User1/xp--${getTimestamp()}/';
    File resultsFile = await localDirFile(add, 'results.txt');
    resultsFile.writeAsStringSync(
        '{"memory gain":${(100 * (initialSize - modeledSize)) / initialSize}, "normal time": $normalPOITime, "opti time": $optiPOITime, "speed-up": ${normalPOITime / optiPOITime}, "diff POI": ${poi.length - modeledPOI.length}}');

    _operation = 0;
    setState(() {});
    debugPrint('Experiment on privamov user 1: done');
    return;
  }

  /// ********** LOADING THE FULL Privamov DATASET **************
  void _doLoadPrivamov() async {
    if (_operation != 0) {
      return;
    }
    _operation = 4;
    setState(() {});
    debugPrint('Starting experiment on privamov full');

    Map<int, GPSDB> gpsDataset = {};
    GPSDB userModel;
    int numberOfPoints = 0;

    final path = await localPath;
    final datasetFileName = '$path/$_datasetsAdd$_privamovFileName';
    final datasetFile = File(datasetFileName);
    List<String> words;
    int userID;
    Stream<String> lines = datasetFile
        .openRead()
        .transform(utf8.decoder) // Decode bytes to UTF-8.
        .transform(const LineSplitter()); // Convert stream to individual lines.
    try {
      await for (var line in lines) {
        words = line.split(';');
        userID = int.parse(words[0]);
        userModel = gpsDataset.putIfAbsent(userID, () => GPSDB());

        /// TODO: check the order
        userModel.add(double.parse(words[3]), double.parse(words[1]),
            double.parse(words[2]));
        numberOfPoints++;
      }
      debugPrint('Dataset privamov is now closed.');
    } catch (e) {
      _operation = 0;
      setState(() {});
      debugPrint('Error while loading the dataset: $e');
      debugPrint('Experiment on privamov full: aborted');
      return;
    }
    debugPrint('Privamov loaded: ${gpsDataset.length} users');

    double initialSize = numberOfPoints * 3;
    double modeledSize = 0;
    for (var entry in gpsDataset.entries) {
      modeledSize += entry.value.getSize();
    }

    /// Printing the results
    String add = '${_addResults}Privamov/Full/xp--${getTimestamp()}/';
    File resultsFile = await localDirFile(add, 'results.txt');
    resultsFile.writeAsStringSync(
        '{"memory gain":${(100 * (initialSize - modeledSize)) / initialSize}}');

    _operation = 0;
    setState(() {});
    debugPrint('Experiment on privamov full: done');
    return;
  }

  @override
  Widget build(BuildContext context) {
    const TextStyle btnTxtStyle = TextStyle(color: Colors.white);

    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            const Text(
              'Choose the experiment:',
            ),

            /// Launch XP: storing cabspotting with FLI and using POI and Promesse
            ElevatedButton(
              style: ElevatedButton.styleFrom(
                backgroundColor: Colors.blue,
              ),
              onPressed: _doCabspottingXP,
              child: const Text('Cabspotting: storage + POI + Promesse',
                  style: btnTxtStyle),
            ),

            /// Launch XP: same as before but with different error parameter
            ElevatedButton(
              style: ElevatedButton.styleFrom(
                backgroundColor: Colors.blue,
              ),
              onPressed: _doCabspottingXPError,
              child: const Text('Cabspotting: changing the error',
                  style: btnTxtStyle),
            ),

            /// Launch XP: computing POI with and without our recursive approach
            ElevatedButton(
              style: ElevatedButton.styleFrom(
                backgroundColor: Colors.blue,
              ),
              onPressed: _doCabspottingXPPOIAttack,
              child: const Text('Cabspotting: using our new POI attack',
                  style: btnTxtStyle),
            ),

            /// Launch XP: ???
            ElevatedButton(
              style: ElevatedButton.styleFrom(
                backgroundColor: Colors.blue,
              ),
              onPressed: _doPrivamovUser1,
              child: const Text('Compute POI on privamov user 1',
                  style: btnTxtStyle),
            ),

            /// Launch XP: ???
            ElevatedButton(
              style: ElevatedButton.styleFrom(
                backgroundColor: Colors.blue,
              ),
              onPressed: _doLoadPrivamov,
              child:
                  const Text('Load privamov full dataset', style: btnTxtStyle),
            ),
            Text(
              _getNameXP(_operation),
            ),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: _interruptXP,
        tooltip: 'Stop the current experiment',
        backgroundColor: Colors.red,
        child: const Icon(Icons.clear_rounded, color: Colors.white),
      ),
    );
  }
}
